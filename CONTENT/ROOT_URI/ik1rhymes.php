<div class="container">
	<h1 class="text-center" style="font-weight: bold;">IK1 Rhymes </h1>
	<div class="text-center">
		<!-- <a href="ik1term1" class="btn btn-primary">Term I</a> -->
		<!-- <a href="ik1term3" class="btn btn-danger">Term III</a> -->
		<!-- <a href="ik1term2" class="btn btn-danger">Term II</a> -->
	</div><br>
	<div class="imageViewer">
		<h3 class="text-center font-style-bold">Cover Image</h3>
		<?php 
			$ik1Cover = glob("IMAGES/rhymes/IK1/cover/*.*");
			foreach ($ik1Cover as $key => $fileName) {  ?>
				<img class="lpanImage" src="<?php echo $fileName; ?>">

			<?php }
		 ?>
	</div><hr>
	<div class="imageViewer" onscroll="myFunction();">
		<h3 class="text-center font-style-bold">Rhymebook</h3>
		<?php 
			$ik1Rhymes = glob("IMAGES/rhymes/IK1/rhymebook/*.*");
			// print_r($ik1Rhymes);
			foreach ($ik1Rhymes as $key => $fileName) {  ?>
				<img class="lpanImage" src="<?php echo $fileName; ?>">

			<?php }
		 ?>
	</div>
	<button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>
</div>

