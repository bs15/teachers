<div class="container">
	<h1 class="text-center" style="font-weight: bold;">PG Lesson Plan(Term II)</h1>
	<div class="text-center">
		<a href="pgterm1" class="btn btn-danger">Term I</a>
		<a href="pgrhymes" class="btn btn-dark">Rhymes</a>
		<a href="pgalphabet" class="btn btn-primary">Alphabet Book</a>
		<a href="pgnumber" class="btn btn-warning">Number Book</a>
	</div><br>
	<div class="imageViewer">
		<?php 
			// $pg = glob("IMAGES/pglessonplan/term2/*.*");
			// print_r($pg);
			foreach ($pg as $key => $fileName) {  ?>
				<img class="lpanImage" src="<?php echo $fileName; ?>">

			<?php }
		 ?>
	</div>
	<button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>
</div>