<div class="container">
	<h1 class="text-center" style="font-weight: bold;">IK3 Rhymes </h1>
	<div class="text-center">
		<!-- <a href="ik3term1" class="btn btn-primary">Term I</a> -->
		<!-- <a href="ik1term3" class="btn btn-danger">Term III</a> -->
		<!-- <a href="ik3term2" class="btn btn-danger">Term II</a> -->
	</div><br>
	<div class="imageViewer">
		<h3 class="text-center font-style-bold">Cover Image</h3>
		<?php 
			$ik3Cover = glob("IMAGES/rhymes/IK3/cover/*.*");
			foreach ($ik3Cover as $key => $fileName) {  ?>
				<img class="lpanImage" src="<?php echo $fileName; ?>">

			<?php }
		 ?>
	</div><hr>
	<div class="imageViewer" onscroll="myFunction();">
		<h3 class="text-center font-style-bold">Rhymebook</h3>
		<?php 
			$ik3Rhymes = glob("IMAGES/rhymes/IK3/rhymebook/*.*");
			// print_r($ik3Rhymes);
			foreach ($ik3Rhymes as $key => $fileName) {  ?>
				<img class="lpanImage" src="<?php echo $fileName; ?>">

			<?php }
		 ?>
	</div>
	<button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>
</div>

