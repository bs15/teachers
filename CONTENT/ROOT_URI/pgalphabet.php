<div class="container">
	<h1 class="text-center" style="font-weight: bold;">Alphabet Book</h1>
	<div class="text-center">
		<!-- <a href="pgterm1" class="btn btn-primary">Term I</a> -->
		<!-- <a href="pgterm3" class="btn btn-danger">Term III</a> -->
		<!-- <a href="pgterm2" class="btn btn-danger">Term II</a> -->
		<a href="pgrhymes" class="btn btn-dark">Rhymes</a>
		<a href="pgnumber" class="btn btn-warning">Number Book</a>
	</div><br>
	<div class="imageViewer" onscroll="myFunction();">
		<?php 
			$pgRhymes = glob("IMAGES/pg_alphabet/*.*");
			// print_r($pgRhymes);
			foreach ($pgRhymes as $key => $fileName) {  ?>
				<img class="lpanImage" src="<?php echo $fileName; ?>">

			<?php }
		 ?>
	</div>
	<button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>
</div>

