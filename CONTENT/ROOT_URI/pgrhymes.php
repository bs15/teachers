<div class="container">
	<h1 class="text-center" style="font-weight: bold;">PG Rhymes </h1>
	<div class="text-center">
		<!-- <a href="pgterm1" class="btn btn-primary">Term I</a> -->
		<!-- <a href="pgterm3" class="btn btn-danger">Term III</a> -->
		<!-- <a href="pgterm2" class="btn btn-danger">Term II</a> -->
		<a href="pgalphabet" class="btn btn-dark">Alphabet Book</a>
		<a href="pgnumber" class="btn btn-warning">Number Book</a>
	</div><br>
	<div class="imageViewer">
		<h3 class="text-center font-style-bold">Cover Image</h3>
		<?php 
			$pgCover = glob("IMAGES/rhymes/PG/cover/*.*");
			foreach ($pgCover as $key => $fileName) {  ?>
				<img class="lpanImage" src="<?php echo $fileName; ?>">

			<?php }
		 ?>
	</div><hr>
	<div class="imageViewer" onscroll="myFunction();">
		<h3 class="text-center font-style-bold">Rhymebook</h3>
		<?php 
			$pgRhymes = glob("IMAGES/rhymes/PG/rhymebook/*.*");
			// print_r($pgRhymes);
			foreach ($pgRhymes as $key => $fileName) {  ?>
				<img class="lpanImage" src="<?php echo $fileName; ?>">

			<?php }
		 ?>
	</div>
	<button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>
</div>

